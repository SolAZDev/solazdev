export default {
    name: "Carlos E. Orama De Jesus",
    title: "Game & Web Developer",
    email: "SolAZDev@gmail.com",
    phone: "939-244-2231",
    address: "PO Box 1126, Gurabo PR, 00778",
    currentObjective: "A Freelance Game designer and web developer, who's looking for a position where I can use the best of my abilities to push the projects forward, and meet or even exceed the company's goals and expectations proficiently. With the ability to reuse existing bases and creating new models, be it for Games, Software, 3D Models, or Web Pages. Looking for opportunities for pers onal contribution and professional growth.",
    majorSkills: [
        "Highly Motivated",
        "Strong Attention to Detail",
        "Front-End Web Design & Development",
        "C#, Typescript, Bash, Linux OS",
        "3D Modeling & Printing",
        "Game Development with Unity",
        "Set & Level Design",
        "Video Editing & Key-frame Animation",
        "Fully Bilingual: English & Spanish"
    ],
    minorSkills: [
        "UX & UI Design",
        "Code Documentation with Doxygen",
        "Story Writing and Storyboarding",
        "Multi-Phase Planning"
    ],
    education: [
        {
            "institute": "Atlantic University College",
            "years": "2011-2015",
            "degree": "Bachelor's Degree in Video Game Design & Development"
        }
    ],
    awards: [
        {
            "award": "Eagle Scout Rank",
            "from": "Boy Scouts of America (T432)"
        },
        {
            "award": "Best Use Of Theme",
            "from": "PR Global Game Jam 2018"
        }
    ],
    workExperience: [
        {
            "job": "Jr Software Developer",
            "employer": "PRSoft, Inc",
            "location": "Santurce, PR",
            "time": "October 2019 - January 2020",
            "resp": [
                "Responsible with developing new parts of their web app, PRCorp",
                "In Charge of developing the backend for new parts in F# and PostgreSQL "
            ]
        },
        {
            "job": "Full Stack Developer",
            "employer": "ifIdea, LLC",
            "location": "Guaynabo, PR",
            "time": "November 2018 - May 2019",
            "resp": [
                "Responsible Developing an Ecosystem for a Client company",
                "In Charge of developing the Administration Website and Employe Mobile App with Angular and Ionic",
                "In Designing the Structured Query Language(SQL) Database, and PHP: Hypertext Preprocessor Middleware as a  Backend"
            ]
        },
        {
            "job": "Lead Game Developer",
            "employer": "Require Technology PR, Inc",
            "location": "Guaynabo, PR",
            "time": "February 2018 - October 2018",
            "resp": [
                "I was in charge of the game known as Genial Skills Web",
                "This included server communication, avatars, quizzes and games amongst other things. ",
                "Extensive Game Testing.",
                "With my contribution, the product is now on the market."
            ]
        },
        {
            "job": "Enviromental Designer & VR Experince Programmer",
            "employer": "SI-3D Media",
            "location": "Santurce, PR",
            "time": "December 2016 - February 2017",
            "resp": [
                "Responsible for creating promotional commercial for a client in Virtual Reality (VR)",
                "Developed 2 promotional games in VR"

            ]
        },
        {
            "job": "Video Game Design Instructor",
            "employer": "Escuela Mariano Abril Intermedia",
            "location": "Guaynabo, PR",
            "time": "February 2016 - May 2016",
            "resp": [
                "In charge of teaching students the principles of 3D Modeling and Game Programming in C#",
                "Student Mentor for creative processes"
            ]
        },
    ]
}